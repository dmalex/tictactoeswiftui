//
//  Tic_Tac_toeApp.swift
//  Tic_Tac_toe
//
import SwiftUI

@main
struct Tic_Tac_toeApp: App
{
    var body: some Scene {
        WindowGroup {
            GameView()
        }
    }
}
